package matlabdriver

import com.mathworks.matlab.types.Struct
import org.json.JSONObject
import org.junit.jupiter.api.Test

class DataConverterTest {
    @Test
    fun convertJsontoStruct() {
        val json = "{\n" +
                "  \"firstName\": \"Bidhan\",\n" +
                "  \"lastName\": \"Chatterjee\",\n" +
                "  \"age\": 40,\n" +
                "  \"address\":\n" +
                "\t\t\t  {\n" +
                "  \"streetAddress\": \"144 J B Hazra Road\",\n" +
                "  \"city\": \"Burdwan\",\n" +
                "  \"state\": \"Paschimbanga\",\n" +
                "  \"postalCode\": \"713102\"\n" +
                "\t\t\t  },\n" +
                "  \"phoneNumber\":\n" +
                "\t\t\t  [\n" +
                "\t\t\t  {\n" +
                "  \"type\": \"personal\",\n" +
                "  \"number\": \"09832209761\"\n" +
                "\t\t\t  },\n" +
                "\t\t\t  {\n" +
                "  \"type\": \"fax\",\n" +
                "  \"number\": \"91-342-2567692\"\n" +
                "\t\t\t  }\n" +
                "\t  \t\t  ]\n" +
                " }\n"
        val result = DataConverter.jsonToStruct(JSONObject(json))
        //printStruct(result)
    }

    fun printStruct (struct: Struct) {
        for (item in struct) {
            println(item.key)
            val value = item.value
            println(value)
            println(value::class.java)
            if (value is Struct) {
                println("--------")
                printStruct(value)
                println("--------")
            }
            else if (value is Array<*>) {
                println("[")
                for (element in value) {

                    if (element is Struct) {
                        printStruct(element)
                    }
                    else {
                        println(element)
                    }
                    println(",")
                }
                println("]")
            }
        }
    }
}