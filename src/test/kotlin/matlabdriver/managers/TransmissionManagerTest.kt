package matlabdriver.managers

import org.junit.jupiter.api.Test
import matlabdriver.parameters.SimulationParameters
import matlabdriver.parameters.TestbedParameters

class TransmissionManagerTest {
    private val transmissionManager = TransmissionManager()

    //Valid matlabdriver.models.parameters
    val imagePath = "/home/tod/Pictures/demons/testimage.bmp"
    val compareWithDigital = false
    val snr = 10.0
    val channelType = "itur3GIAx"
    val transmissionType = "static"
    val txGain = 60.0
    val rxGain = 40.0
    val carrierFrequency = 2600



    @Test
    fun `successfully launches a simulation transmission` () {

        val parameters = SimulationParameters(
                imagePath = imagePath,
                compareWithDigital = compareWithDigital,
                snr = snr,
                channelType = channelType,
                transmissionType = transmissionType
        )
        transmissionManager.launchTransmission(parameters)
    }
/*
    @Test
    fun `successfully launched a testbed transmission` () {
        val parameters = TestbedParameters(
                imagePath = imagePath,
                compareWithDigital = compareWithDigital,
                txGain = txGain,
                rxGain = rxGain,
                carrierFrequency = carrierFrequency
        )
        transmissionManager.launchTransmission(parameters)
    }
    */

    /*

    @Test
    fun `fails to launch a testbed transmission if the testbed is not running` () {
        val matlabdriver.models.parameters = TestbedParameters(
                imagePath = imagePath,
                compareWithDigital = false,
                txGain = txGain,
                rxGain = rxGain,
                carrierFrequency = carrierFrequency
        )
        transmissionManager.launchTransmission(matlabdriver.models.parameters)
    }*/


}

