package matlabdriver.parameters


// SIMULATION ENVIRONMENT CONSTANTS
const val MIN_SNR_VALUE = 0.0
const val MAX_SNR_VALUE = 100.0
val SUPPORTED_SIMULATION_CHANNEL_TYPES= arrayOf(
        "itur3GIAx", "itur3GIBx"
)
val SUPPORTED_SIMULATION_TRANSMISSION_TYPES = arrayOf(
        "static",
        "quasi-static",
        "pedestrian"
)
const val SIMULATION_ENVIRONMENT_ID = 0
const val TESTBED_ENVIRONMENT_ID = 1

//TESTBED ENVIRONRMENT CONSTANTS

const val MIN_TX_GAIN_VALUE = 0.0
const val MAX_TX_GAIN_VALUE = 100.0
const val MIN_RX_GAIN_VALUE = 0.0
const val MAX_RX_GAIN_VALUE = 100.0
const val MIN_CARRIER_FREQUENCY_VALUE = 80
const val MAX_CARRIER_FREQUENCY_VALUE = 6000


