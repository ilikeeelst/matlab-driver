package matlabdriver.parameters

class ValueOutOfRangeException(message: String): Exception(message)

class ImageFileNotValidException(message: String): Exception(message)