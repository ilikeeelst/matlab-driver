package matlabdriver.parameters

import matlabdriver.imageFileIsValid


abstract class TransmissionParameters(
        val imagePath: String,
        val compareWithDigital: Boolean
) {
    init {
        if (!imageFileIsValid(imagePath)) {
            throw ImageFileNotValidException("The provided images file is does" +
                    " not exists or is not a valid images file")
        }
    }
    abstract fun getParametersArray() : Array<Any>
}

class SimulationParameters (imagePath: String,
                            compareWithDigital: Boolean,
                            val snr: Double,
                            val channelType: String,
                            val transmissionType: String)
    : TransmissionParameters(imagePath, compareWithDigital) {

    init {
        if (! SUPPORTED_SIMULATION_TRANSMISSION_TYPES.contains(this.transmissionType)) {
            throw NotImplementedError("Selected transmission type: $transmissionType is not supported\n" +
                    "Please choose one of the followings:\n" +
                    "$SUPPORTED_SIMULATION_TRANSMISSION_TYPES")
        }
        if (! SUPPORTED_SIMULATION_CHANNEL_TYPES.contains((this.channelType))) {
            throw NotImplementedError("Selected channel type: ${channelType}is not supported\n" +
                    "Please choose one of the followings:\n" +
                    "$SUPPORTED_SIMULATION_CHANNEL_TYPES")
        }
        if (snr > MAX_SNR_VALUE || snr < MIN_SNR_VALUE) {
            throw ValueOutOfRangeException(
                    "The supplied value for snr: $snr has to range between" +
                            " $MIN_SNR_VALUE and $MAX_SNR_VALUE")
        }
    }

    override fun getParametersArray(): Array<Any> {
        return arrayOf(
                SIMULATION_ENVIRONMENT_ID,
                imagePath.toCharArray(),
                compareWithDigital,
                doubleArrayOf(snr),
                channelType,
                transmissionType)
    }
}

class TestbedParameters (imagePath: String,
                         compareWithDigital: Boolean,
                         val txGain: Double,
                         val rxGain: Double,
                         val carrierFrequency: Int): TransmissionParameters(imagePath, compareWithDigital) {
    init {
        if (txGain < MIN_TX_GAIN_VALUE || txGain > MAX_TX_GAIN_VALUE) {
            throw ValueOutOfRangeException(
                    "The supplied value for txGain: $txGain has to range between" +
                            " $MIN_TX_GAIN_VALUE and $MAX_TX_GAIN_VALUE"
            )
        }
        if (rxGain < MIN_RX_GAIN_VALUE || rxGain > MAX_RX_GAIN_VALUE) {
            throw ValueOutOfRangeException(
                    "The supplied value for rxGain: $rxGain has to range between" +
                            " $MIN_RX_GAIN_VALUE and $MAX_RX_GAIN_VALUE"
            )
        }
        if (carrierFrequency < MIN_CARRIER_FREQUENCY_VALUE
                || carrierFrequency > MAX_CARRIER_FREQUENCY_VALUE) {
            throw ValueOutOfRangeException(
                    "The supplied value for carrierFrequency: $carrierFrequency" +
                            " has to range between $MIN_CARRIER_FREQUENCY_VALUE" +
                            " and $MAX_CARRIER_FREQUENCY_VALUE"
            )
        }
    }

    override fun getParametersArray(): Array<Any> {
        return arrayOf(
                TESTBED_ENVIRONMENT_ID,
                imagePath.toCharArray(),
                compareWithDigital,
                txGain,
                rxGain,
                carrierFrequency)
    }
}