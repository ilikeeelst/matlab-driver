package matlabdriver.models

import com.mathworks.matlab.types.Struct
import com.mathworks.matlab.types.Complex
import java.io.File

class TransmissionResult (fevalOutput: Array<Any>) {

    init {
        if (fevalOutput.isEmpty()) {
            throw UnexpectedMatlabFunctionOutputFormatException(
                    "The given feval output doesn't match the expected format: \n"
                    + "The array fevalOutput cannot be empty")
        }
    }

    private val rootObj = fevalOutput[0]
    private val rootStruct: Struct
    val calculatedSNRs: List<List<Double>>
    val inputSNRs : List<Double>
    val ssims : List<List<Double>>
    val sdrs : List<List<Double>>
    val channels : List<Channel>
    val images : List<File>
    val methods : List<Method>
    val numberOfMethods : Int

    init {
        if (rootObj is Struct) {
            rootStruct = rootObj
        }
        else {
            throw UnexpectedMatlabFunctionOutputFormatException(
                    "The given feval output doesn't match the expected format:\n" +
                            "The first element of the fevalOutputArray is not a Struct")
        }
        calculatedSNRs = calculatedSNRs()
        inputSNRs = inputSNRs()
        ssims = ssims()
        sdrs = sdrs()
        images = images()
        channels = channels()
        methods = methods()
        numberOfMethods = methods.size
    }

    private fun getField (field: String) : Any? {
        return rootStruct.get(field)
                ?: throw NoSuchFieldException("The requested field cannot be found in the result")
    }

    private fun methods () : List<Method> {
        val methodsContainer = getField("methods")
        val methods = ArrayList<Method>()
        if (methodsContainer is Array<*>) {
            for (methodNumber in methodsContainer.indices) {
                val row = methodsContainer[methodNumber]
                if (row is Array<*>) {
                    val phases = ArrayList<String>()
                    val SNRs = ArrayList<Snr>()
                    for (item in row) {
                        if (item is String) {
                            phases.add(item)
                        }
                        else {
                            throw UnexpectedMatlabFunctionOutputFormatException(
                                    "The methods field contains an unexpected null value")
                        }
                    }
                    for (i in this.inputSNRs.indices) {
                        SNRs.add(Snr(
                                calculatedSNR = this.calculatedSNRs[i][methodNumber],
                                ssim = this.ssims[i][methodNumber],
                                sdr = this.sdrs[i][methodNumber],
                                outputImageFile = this.images[methodNumber],
                                channel = this.channels[methodNumber]
                        ))
                    }
                    methods.add(Method(strategy = phases, SNRs = SNRs))
                }
            }
        }
        return methods
    }

    private fun inputSNRs (): List<Double> {
        val snrsContainer = getField("SNRdBvec")
        val inputSNRs = arrayListOf<Double>()
        if (snrsContainer is Array<*>) {
            for (item in snrsContainer) {
                if (item is Double) {
                    inputSNRs.add(item)
                }
                else {
                    throw UnexpectedMatlabFunctionOutputFormatException(
                            "The SNR input values format is unexpected")
                }
            }
            return inputSNRs
        }
        else {
            if(snrsContainer is Double) {
                return listOf(snrsContainer)
            }
            else{
                throw UnexpectedMatlabFunctionOutputFormatException(
                        "The SNR input values format is not a Double or an array of Doubles")
            }
        }
    }

    private fun calculatedSNRs (): List<List<Double>> {
        return getTransmissionvalue("SNR_dB_calc_rx")
    }

    private fun ssims(): List<List<Double>> {
        return getTransmissionvalue("ssimval_0")
    }

    private fun sdrs(): List<List<Double>> {
        return getTransmissionvalue("sdrval_0")
    }

    private fun getTransmissionvalue(fieldName: String) : List<List<Double>>{
        val container = getField(fieldName)
        val values = mutableListOf<List<Double>>()
        if (container is Array<*>) {
            for (row in container) {
                if (row is DoubleArray) {
                    values.add(row.asList())
                }
                else {
                    throw  UnexpectedMatlabFunctionOutputFormatException(
                            "The calculated SNR field has unexpected format")
                }
            }
        }
        return values
    }

/*
    private fun images() : Array<Array<Double>> {
        //val images = arrayListOf<List<Double>>()
        for (i in 0..numberOfMethods) {
            val imageMatricesContainer = getField("sout_$i")
            //val imageMatrix = arrayListOf<ArrayList<Double>>()
            if (imageMatricesContainer is Array<*>) {
                val imageMatrixContainer = imageMatricesContainer[0]
                if (imageMatrixContainer is Array<*>) {
                    for (row in imageMatrixContainer) {
                        if (row is DoubleArray) {
                            for (item in row) {
                            }
                        }
                    }
                }
            }
            else {
                throw UnexpectedMatlabFunctionOutputFormatException("The images matrix field has an unexpected format")
            }
        }
        return arrayOf(arrayOf(1.0), arrayOf(2.0))
    }

*/


    private fun images() : List<File> {
        val outputDirectory = "/home/tod/gtec/demoproject/demoajscc/analog_system/data_output/iteration_1/data/images/"
        val dir = File(outputDirectory)
        val files = dir.listFiles()
        val imageFileMethod1 = files.filter { it.name.contains("expansion-expansion-uncoded-none") }[0]
        val imageFileMethod2 = files.filter { it.name.contains("expansion-uncoded-uncoded-none") }[0]
        return listOf(imageFileMethod1, imageFileMethod2)
    }



    private fun channels() : List<Channel> {
        val channelContainer = getField("estimated_channel_3d")
        if (channelContainer is Array<*>) {
            val channels = arrayListOf<Channel>()
            for (channelData in channelContainer) {
                if (channelData is Struct) {
                    val _timeValues = channelData["axis_time"]
                    val _frequencyValues = channelData["axis_freq"]
                    val _values = channelData["values"]
                    val timeValues : MutableList<Double>
                    val frequencyValues : MutableList<Double>
                    val indexesToDelete = arrayListOf<Int>()
                    if (_timeValues is DoubleArray && _frequencyValues is DoubleArray) {
                        timeValues = _timeValues.toMutableList()
                        frequencyValues = _frequencyValues.toMutableList()
                    }
                    else {
                        throw UnexpectedMatlabFunctionOutputFormatException(
                                "The Channel field has an unexpected format")
                    }
                    if (_values is Array<*>) {
                        assert(_values.size == _frequencyValues.size)
                        val values = arrayListOf<Array<Complex>>()
                        for (i in _values.indices) {
                            val row = _values[i]
                            if (row is Array<*>) {
                                val r = arrayListOf<Complex>()
                                var skipRow = false
                                for (j in row.indices) {
                                    val item = row[j]
                                    if(item is Complex) {
                                        if (item.imag == 0.0 && item.imag == 0.0) {
                                            skipRow = true
                                            indexesToDelete.add(i)
                                            break
                                        }
                                        r.add(item)
                                    }
                                    else {
                                        throw UnexpectedMatlabFunctionOutputFormatException(
                                                "The Channel field has an unexpected format")
                                    }
                                }
                                if (!skipRow) {
                                    values.add(r.toTypedArray())
                                }
                            }
                            else {
                                throw UnexpectedMatlabFunctionOutputFormatException(
                                        "The Channel field has an unexpected format")
                            }
                        }
                        for (i in indexesToDelete.sortedDescending()){
                            frequencyValues.removeAt(i)
                        }
                        assert(frequencyValues.size == values.size)
                        val channel = Channel(
                                timeValues = timeValues.toTypedArray(),
                                frequencyValues = frequencyValues.toTypedArray(),
                                values = values.toTypedArray())
                        channels.add(channel)
                }
            }
            if (channelData is Struct) {

                }else {
                    throw UnexpectedMatlabFunctionOutputFormatException("The Channel field has an unexpected format")
                }
            }
            return channels
        }
        else {
            throw UnexpectedMatlabFunctionOutputFormatException("The Channel field has an unexpected format")
        }
    }
}

class UnexpectedMatlabFunctionOutputFormatException (message: String) : Exception(message)
