package matlabdriver.models

data class Method (val strategy: List<String>, val SNRs: List<Snr>)
