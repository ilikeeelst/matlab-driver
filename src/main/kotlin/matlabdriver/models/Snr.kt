package matlabdriver.models

import java.io.File

data class Snr (
        val calculatedSNR: Double,
        val ssim: Double,
        val sdr: Double,
        val outputImageFile: File,
        val channel: Channel)