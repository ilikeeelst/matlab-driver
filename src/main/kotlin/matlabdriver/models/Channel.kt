package matlabdriver.models

import com.mathworks.matlab.types.Complex


data class Channel (val timeValues: Array<Double>,
                    val frequencyValues: Array<Double>,
                    val values: Array<Array<Complex>>)
