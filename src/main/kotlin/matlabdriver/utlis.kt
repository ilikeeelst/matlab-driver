package matlabdriver

import java.io.File
fun imageFileIsValid(filePath: String) : Boolean {
    val file = File(filePath)
    //TODO for subsequent versions: verify file type
    return file.exists() && (! file.isDirectory)
}

fun clearOutputDirectory (directoryPath: String): Boolean {
    val file = File(directoryPath)
    if (file.exists()) {
        return file.deleteRecursively()
    }
    else {
        return true
    }
}