package matlabdriver.managers

import matlabdriver.clearOutputDirectory
import com.mathworks.engine.MatlabEngine
import matlabdriver.models.TransmissionResult
import matlabdriver.parameters.TransmissionParameters


class TransmissionManager {
    private val engine = MatlabEngine.startMatlabAsync()

    init{
        engine.get().feval<String>("cd", FUNCTIONS_LOCATION.toCharArray())
    }

    fun testBedIsRunning() : Boolean {
        return engine.get().feval(START_NODES_FUNCTION_NUMBER_OF_OUTPUTS, START_NODES_FUNCTION_NAME)
    }

    fun startNodes(): Boolean {
        return engine.get().feval(START_NODES_FUNCTION_NUMBER_OF_OUTPUTS, START_NODES_FUNCTION_NAME)
    }

    fun launchTransmission(transmissionParameters: TransmissionParameters): TransmissionResult {
        val outputDirectoryIsClean = clearOutputDirectory(OUTPUT_DIRECTORY)
        if (outputDirectoryIsClean) {
            try {
                val output = engine.get().feval<Array<Any>>(
                        TRANSMISSION_FUNCTION_NUMBER_OF_OUTPUTS,
                        TRANSMISSION_FUNCTION_NAME,
                        *transmissionParameters.getParametersArray())
                return TransmissionResult(output)
            }
            catch (e: Exception) {
                throw RuntimeException("An error occurred while executing the matlab function", e)
            }
            finally {

            }
        }
        else{
            throw RuntimeException("Could not clear output directory")
            //TODO for subsequent versions: maybe create a new directory without stopping the execution
        }

    }
}