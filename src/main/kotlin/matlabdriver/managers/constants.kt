package matlabdriver.managers

val TRANSMISSION_FUNCTION_NUMBER_OF_OUTPUTS = 1
val START_NODES_FUNCTION_NUMBER_OF_OUTPUTS = 1
val TRANSMISSION_FUNCTION_NAME = "demo_balsa_iterate_script"
val START_NODES_FUNCTION_NAME = "startNodes"
val FUNCTIONS_LOCATION = "/home/tod/gtec/demoproject/demoajscc/analog_system"
val OUTPUT_DIRECTORY = "/home/tod/gtec/demoproject/demoajscc/analog_system/data_output"