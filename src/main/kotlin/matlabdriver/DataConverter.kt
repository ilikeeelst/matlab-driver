package matlabdriver

import com.mathworks.matlab.types.Struct
import org.json.JSONArray
import org.json.JSONObject

class DataConverter {

    companion object {
        fun structToMap(struct : Struct) : Map<String, Any?> {
            val map = mutableMapOf<String, Any?>()
            for (key in struct.keys) {
                map[key] = struct[key]
            }
            return map
        }


        fun jsonToStruct (json: JSONObject): Struct {
            val jsonAsSet = mutableSetOf<Any>()
            for (fieldName in json.keys()) {
                println(fieldName)
                jsonAsSet.add(fieldName)
                val value = json[fieldName]
                println(value)
                println(value::class.java)
                when (value) {
                    is JSONObject -> jsonAsSet.add(jsonToStruct(value))
                    is JSONArray -> {
                        val list = mutableListOf<Any>()
                        for (item in value) {
                            if (item is JSONObject) {
                                list.add(jsonToStruct(item))
                            }
                            else {
                                list.add(item)
                            }

                        }
                        jsonAsSet.add(list.toTypedArray())
                    }
                    else -> jsonAsSet.add(value)
                }
            }
            val struct = Struct(*jsonAsSet.toTypedArray())
            return struct
        }
    }
}